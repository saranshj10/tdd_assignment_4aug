import unittest
import Area_Of_Circle
myarea = Area_Of_Circle.area()


class Test_area(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print("Executing Class")

    def setUp(self):
        print("Executing test cases")

    def test_findArea_positive(self):
        self.assertEqual(myarea.findArea(7),154)
        self.assertEqual(myarea.findArea(1), 3)
        self.assertEqual(myarea.findArea(10), 314)

    def test_findArea_Zero(self):
        self.assertEqual(myarea.findArea(0), 0)

    def test_findArea_negative(self):
        #self.assertRaises(ValueError)
        with self.assertRaises(ValueError):
            myarea.findArea(-5)

    def test_findArea_Document(self):
        with patch('requests.get') as mocked_get:
            mocked_get.return_value.ok = True
            mocked_get.return_value.text = 'Success'

            link="https://www.vedantu.com/maths/area-of-a-circle"

            schedule = myarea.AreaCalculationDocument(link)
            mocked_get.assert_called_with(link)
            self.assertEqual(schedule, "Documentation is Verified")

            mocked_get.return_value.ok = False

            schedule = myarea.AreaCalculationDocument(link)
            mocked_get.assert_called_with(link)
            self.assertEqual(schedule, "Document is Not Verified")

    def tearDown(self):
        print("Stopping the running test cases")

    @classmethod
    def tearDownClass(cls):
        print("Program executed succesfully")


        
if __name__ == '__main__':
    unittest.main()
