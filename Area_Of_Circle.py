from Resources import input_file
import requests

class area():
    def findArea(self, r):
        PI = 3.142
        return round(PI * (r * r))

    def AreaCalculationDocument(self, link):
        response = requests.get(f'{link}')
        if response.ok:
            return "Documentation is Verified"
        else:
            return "Document is Not Verified"

radius = input_file.myradius
if radius < 0:
    raise ValueError("Radius Can't be Negative")
circlearea = area()
area_circle = circlearea.findArea(radius)
print(area_circle)